/*
 * Classe de gestion du PID
 * @author: KLIPFEL Arthur
 * @version 0.1
 * created on 08/10/2016
 */
 
class PID{
public:
  PID(float p=0,float i=0,float d=0){
    kp=p;
    ki=i;
    kd=d;
    setpoint=0;
    intError=0;
    preError=0;
  }
  float compute(float angle,float dt){
    float derError,error;
    error=angle-setpoint;
    if(ki!=0){
      intError+=error*dt;
    }
    else{
      intError=0;
    }
    derError=(error-preError)/dt;
    output=kp*error+ki*intError+kd*derError;
    preError=error;
    return output;
  }
  float getOutput(){
    return output;
  }
  float setpoint;
  float kp;
  float ki;
  float kd;
protected:
  float intError;
  float preError;
  float output;
};

