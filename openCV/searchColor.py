import numpy as np
import cv2

cap = cv2.VideoCapture(0)

while(True):
	# Capture frame-by-frame
	ret, frame = cap.read()
	image=frame.copy()
	#gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	hsv = cv2.cvtColor(image , cv2.COLOR_BGR2HSV)
	min=np.array([105,100,100])
	max=np.array([125,255,255])
	gray=cv2.inRange(hsv,min,max)
	
	rows,cols = gray.shape
	count=0
	[x,y]=[0,0]
	for i in range(rows/4):
		for j in range(cols/4):
			if gray[i*4,j*4]==255:
				x=x+j*4
				y=y+i*4
				count=count+1
	if count!=0:
		x=x/count
		y=y/count
		cv2.circle(image, (x, y), 10, (0, 255, 0), 4)
	cv2.imshow('frame',image)
	key=cv2.waitKey(1) & 0xFF
	if key == ord('q'):
		break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()