#ifndef R9D_H
#define R9D_H

#include <Arduino.h>
#include <SBUS.h>


class R9D
{
  public:
    R9D(SBUS *sbus);

    void Update();
    void Set_Commands(int *Commands);
    int Get_Left_Joystick_Horizontal();
    int Get_Left_Joystick_Vertical();
    int Get_Right_Joystick_Horizontal();
    int Get_Right_Joystick_Vertical();
    int Get_SwitchA();
    int Get_SwitchB();
    int Get_SwitchC();
    int Get_VRC();
    int Get_VRB();
    
  private:
    //int m_Throttle;
    int m_Left_Joystick_Horizontal;
    int m_Left_Joystick_Vertical; //Throttle
    int m_Right_Joystick_Horizontal;
    int m_Right_Joystick_Vertical;
    int m_SwitchA;
    int m_SwitchB;
    int m_SwitchC;
    int m_VRC; // (Left POTAR)
    int m_VRB; // (RIGHT POTAR)

    SBUS *m_sbus; // [0 - 3] Joysticks , [4 - 6] Switchs , [7 - 8] Knobs
};

#endif
