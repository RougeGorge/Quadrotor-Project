#include<Servo.h>

#include "PID.h"

Servo Moteur;

int Sensor=0;

float RPM=0.0;
float Order=1000.0;
unsigned long Clock=0;

PID PID_Speed(&RPM,&Clock);



volatile unsigned long Counter=0;

double Command=0.0;

void blink()
{
  Counter++;
}

void setup()
{
  Serial.begin(9600);
  //Moteur.attach(8);
  pinMode(8,OUTPUT);
  attachInterrupt(0, blink, RISING); // attache l'interruption externe n°0 à la fonction blink
  
  
  PID_Speed.Set_PID(1.0,0,0);
  PID_Speed.Set_Order(Order);
  
}


void loop()
{
  
  Clock=millis();
  RPM=60.0*1000.0*Counter/(7.0*Clock);
  Command=PID_Speed.Get_Response();  
  Clock=millis()-Clock;
  
  if(Command<=0)
  {
    Command=0.0;
  }
  
  Serial.print(Counter);
  Serial.print("\t");
  Serial.print(RPM);
  Serial.print("\t");
  Serial.println(Command);
 // Moteur.write(180);
  analogWrite(9,Command);
  
}

