#ifndef CF1_H
#define CF1_H

#include <Adafruit_Sensor.h>
#include <Adafruit_LSM9DS0.h>


class CF1
{
  public:
  
    CF1(unsigned long *Clock,float Alpha=0.96);
    void Set_Alpha(float Alpha);
    void Set_Container(float Angle[3]);
    void Set_Verbose(bool Verbose);
    void Set_CSV(bool CSV);
    //void Update(float Data[6]);
    //void Update(float Gyro[3],float Acc[3]);
    void Update(sensors_vec_t Gyro_Vector,sensors_vec_t Acc_Vector,sensors_vec_t Mag);
    float Get_Angle(int IndexYPR);
    void Disp_Angle(sensors_vec_t Mag);
    void Disp_CSV();
    void Disp_DEBUG(sensors_vec_t Gyro);

    static void rotate(float &x,float &y,float a);

  private:
    unsigned long *m_Clock;//t
    
    
    float m_Alpha;

    float m_Gyro_ypr[3];
    float m_Acc_ypr[3];
    
    float m_Filtered_Data[3];
    //float m_Prev_Filtered_Data[3];

    float *m_p_Filtered; //float[3] contains the 3 filtred angles

    bool m_Verbose;
    bool m_CSV;
    bool m_Debug;

    //double m_TestIntegrator;
  
};

#endif
