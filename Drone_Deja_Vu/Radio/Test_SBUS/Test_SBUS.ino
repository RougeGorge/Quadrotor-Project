
//ALL CREDITS TO https://github.com/zendes/SBUS


// The Arduino Pro Mini is a simple & cheap board, ideal for leaving inside of
// your model. It only has a single Serial port, and the default SBUS code
// doesn't run on it. You can get it to work by not using the timer, but that
// means you can't use delay in the loop() function anymore.

#include <SBUS.h>
SBUS sbus(Serial);


int CH1=3;
int CH2=5;
int CH3=6;
int CH4=9;
int CH5=10;
int CH6=11;

void setup()
{
  sbus.begin(false);
}

void loop()
{
  sbus.process();
  int V1 = sbus.getChannel(7);
  int V2 = sbus.getChannel(8);
  int V3 = sbus.getChannel(9);
  int V4 = sbus.getChannel(10);
  int V5 = sbus.getChannel(11);
  int V6 = sbus.getChannel(12);
  
  analogWrite(CH1, V1 / 8);
  analogWrite(CH2, V2 / 8);
  analogWrite(CH3, V3 / 8);
  analogWrite(CH4, V4 / 8);
  analogWrite(CH5, V5 / 8);
  analogWrite(CH6, V6 / 8);
}
